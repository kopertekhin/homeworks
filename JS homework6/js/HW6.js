const filterBy = function(data, dataType) {
    if(data.some(function(item){
        return typeof(item) !== dataType;
    })){
        let dataCleared = data.filter(function(element) {
            return typeof(element) === dataType;
        });
        return dataCleared;
    } return data;
}


dataType = 'string';
testData = ["hello", "world", 23, "23", null];

x = filterBy(testData, dataType);
console.log(x);