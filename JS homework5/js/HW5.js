function createNewUser(fName, lName, bDay) {
    let newUser = new Object();

    Object.defineProperties(newUser, {
        'firstName': {
            get() {
                return this._firstName;
            },
            set(value) {
                /^[a-zA-Z]+$/i.test(value) ? this._firstName = value : this._firstName = false;
            }
        },
        'lastName': {
            get() {
                return this._lastName;
            },
            set(value) {
                /^[a-zA-Z]+$/i.test(value) ? this._lastName = value : this._lastName = false;
            }
        },
        'birthday': {
            get() {
                return this._birthday;
            },
            set(value) {
                let datePattern = /^(0?[1-9]|[12][0-9]|3[01])[\/\-.](0?[1-9]|1[012])[\/\-.]\d{4}$/i;
                datePattern.test(value) ? this._birthday = new Date(value.slice(6), value.slice(3,5), value.slice(0,2)) : this._birthday = false;
            }
        },
        'getLogin': {
            value: function(){
                if([this.firstName, this.lastName].some(function(item){
                    return item === false
                })){
                    return false
                }
                return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
            },
            writable: false
        },
        'getAge': {
            value: function(){
                if(!this.birthday){
                    return false
                }
                let dateCurrent = new Date();
                return (dateCurrent - this.birthday)/31536000000 | 0;
            },
            writable: false
        },
        'getPassword': {
            value: function(){
                if([this.firstName, this.lastName].some(function(item){
                    return item === false
                }) || !this.birthday){
                    return false
                }
                let passwordLeft = this.firstName[0].toUpperCase() + this.lastName.toLowerCase();
                let passwordRight = this.birthday.getFullYear();
                return passwordLeft + passwordRight;
            },
            writable: false
        }
    });

    let userFirstName = fName || prompt("What is your first name?");
    newUser.firstName = userFirstName;
    
    let userLastName = lName || prompt("What is your last name?");
    newUser.lastName = userLastName;

    let userBDay = bDay || prompt("When is your birthday? Please enter the value in dd.mm.yyyy format");
    newUser.birthday = userBDay;

    return newUser
}


let myUser = createNewUser();
console.log(`new user age: ${myUser.getAge()}`);
console.log(`new user login: ${myUser.getLogin()}`);
console.log(`new user password: ${myUser.getPassword()}`);
