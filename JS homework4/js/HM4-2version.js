function createNewUser(fName, lName) {
    let newUser = new Object();
    Object.defineProperty(newUser, 'firstName', {
        get() {
            return this._firstName;
        },
        set(value) {
            if(value === null){
                alert('Your value is null. Try again.')
            }else if(value === ""){
                alert('Your value is undefined. Try again.')
            }
            this._firstName = value;
        }
    });
    Object.defineProperty(newUser, 'lastName', {
        get() {
            return this._lastName;
        },
        set(value) {
            if(value === null){
                alert('OoYour value is null. Try again.')
            }else if(value === ""){
                alert('OoYour value is undefined. Try again.')
            }
            this._lastName = value;
        }
    });
    Object.defineProperty(newUser, 'getLogin', {
        value: function(){
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        writable: false
    })

    let userFirstName = fName || prompt("What is your first name?");
    newUser.firstName = userFirstName;
    
    let userLastName = lName || prompt("What is your last name?");
    newUser.lastName = userLastName;

    return newUser
}


myUser = createNewUser();
myLogin = myUser.getLogin();
console.log(myUser);
console.log(myLogin);
