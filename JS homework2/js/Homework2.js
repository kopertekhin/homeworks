let enteredNumber = +prompt("Please, enter your number");
    
    if (Number.isInteger(enteredNumber)) {
        for (let i = 1; i <= enteredNumber; i++) {
            if (i%5 === 0) {
                console.log(`This number is multiple of 5 in the range from zero to ${enteredNumber} -> `, i);
            } else if (enteredNumber < 5) {
                console.log("Sorry, no numbers");  
            }
        }
    } else {
        return "Please, enter the integer";
    }


// #### Необязательное задание продвинутой сложности:

let m = +prompt("Please, enter the starting value");
let n = +prompt("Please, enter the ending value");

if (n>m) {
    advanceTask:
    for (let i = m; i <= n; i++) {
        for (let j = 2; j < i; j++) {
            if (i % j === 0) continue advanceTask;
        }
        console.log(`This is a prime number in the range from ${m} to ${n} ->`, i);
    }
} else {
    alert ("Please, enter the smallest number first");
}